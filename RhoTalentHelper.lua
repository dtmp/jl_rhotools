RT_TalentHelper = {}

-- Возвращает массив талантов игрока {{name=>имя,texture=>текстура},...}
RT_TalentHelper.GetList = function()

    local t={}
    for tab = 1,GetNumTalentTabs() do
        for i = 1, GetNumTalents(tab) do
            nameTalent, icon, tier, column, currRank, maxRank= GetTalentInfo(tab,i);

            local talent = {}
            talent.name = nameTalent
            talent.texture = icon
            t[nameTalent] = talent
        end
    end

    return t
end

-- Выводит информацию о талантах игрока
RT_TalentHelper.ShowInfo = function ( talent )
    local t = RT_TalentHelper.GetList()
    for i,s in pairs(t) do
        if (not talent or s.name==talent) then
            RT.Log(s.name..' - '..s.texture)
        end
    end
end

-- Получает текстуру таланта по имени
RT_TalentHelper.GetTexture = function ( talent )

    if not talent then
        if RT.Debug then RT.Log('Не передано имя таланта') end
        return false
    end

    local t = RT_TalentHelper.GetList()
    for i,s in pairs(t) do
        if (s.name==talent) then
            return s.texture
        end
    end

    if RT.Debug then RT.Log('Не найдена текстура для "'..talent..'"') end
    return false
end