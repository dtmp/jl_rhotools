RT_SpellHelper = {}

-- Возвращает массив заклинаний из книги {{name=>имя,texture=>текстура},...}
RT_SpellHelper.GetList = function( book, index )

    if (not book) then book = "spell" end
    if (not index) then index = "name" end

    local t={};
    local i=1;
    while GetSpellName(i,book) do
        local s = {};
        s.slot = i;
        s.name = GetSpellName(i,book);
        s.texture = GetSpellTexture(i,book);
        t[s[index]] = s;

        i = i + 1;
    end

    return t
end

-- Выводит информацию о заклинаниях из книги
RT_SpellHelper.ShowInfo = function ( spell )
    local t = RT_SpellHelper.GetList()
    for i,s in pairs(t) do
        if (not spell or s.name==spell) then
            RT.Log(s.name..' ('..s.slot..') - '..s.texture)
        end
    end
end

-- Получает текстуру заклинания из книги
RT_SpellHelper.GetTexture = function ( spell )
    local t = RT_SpellHelper.GetList()
    for i,s in pairs(t) do
        if (s.name==spell) then
            return s.texture
        end
    end

    if RT.Debug then RT.Log('Не найдена текстура для "'..spell..'"') end
    return false
end

-- Проверяет, готово ли заклинание к использованию (cooldown)
RT_SpellHelper.IsReady = function ( spell )
    local t = RT_SpellHelper.GetList()
    if (not t[spell]) then
        RT.Log('Заклинание '..spell..' не найдено в книге')
        return false
    end

    local start, duration, enabled = GetSpellCooldown(t[spell].slot, "spell");

    if (duration~=0) then
        if RT.Debug then RT.Log('Заклинание "'..spell..'" не готово') end
        return false
    end

    if RT.Debug then RT.Log('Заклинание "'..spell..'" готово к использованию') end
    return 1

end