
-- http://wowwiki.wikia.com/index.php?title=World_of_Warcraft_API&oldid=281620 - API версии 1.12
-- https://quikluacsharp.ru/qlua-osnovy/peremennye-massivy-i-funkcii-v-qlua-lua/ - гайд по LUA
-- http://www.user.su/lua/index.php?id=3 - Справочник по LUA
-- https://habrahabr.ru/post/119312/ - руководство к написанию аддонов

RT = {}
RT.Debug = false


RT.Log = function ( text )
    SELECTED_CHAT_FRAME:AddMessage( text )
end

-- Кастует заклинание, проверяя дистанцию и кулдаун
RT.CastSpell = function ( spell, unit )

    if RT.Debug then RT.Log('Проверка возможности использования "'..spell..'"') end

    if not unit then unit="target" end

    -- Текстура заклинания
    local texture = RT_SpellHelper.GetTexture(spell)

    -- Если у заклинания нет текстуры, не кастуем (т.к. заклинания нет в книге)
    if not texture then
        return false
    end

    -- Проверим кулдаун
    if (not RT_SpellHelper.IsReady(spell)) then
        return false
    end

    -- Проверим дистанцию
    if (not RT_ActionHelper.IsRange(texture)) then
        return false
    end

    if RT.Debug then RT.Log('Кастуем заклинание "'..spell..'"') end
    CastSpellByName(spell)

--    if unit~="player" then
--        CastSpellByName(spell)
--    else
--        CastSpellByName(spell, 1)
--    end

    return 1
end

-- Кастует заклинание, если дебафа еще нет на цели
RT.CastDebuff = function ( spell, unit )
    if not unit then unit="target" end

    -- Текстура заклинания
    local texture = RT_SpellHelper.GetTexture(spell)

    -- Если у заклинания нет текстуры, не кастуем
    if not texture then
        return false
    end

    -- Если дебаф уже есть на цели, не кастуем
    if (RT_DebuffHelper.Check(unit, texture)) then
        return false
    end

    return RT.CastSpell(spell, unit)
end

-- Кастует заклинание, если бафа еще нет на игроке
RT.CastBuff = function ( spell, unit )
    if not unit then unit="player" end

    -- Текстура заклинания
    local texture = RT_SpellHelper.GetTexture(spell)
    -- Если у заклинания нет текстуры, не кастуем
    if not texture then
        if RT.Debug then RT.Log('Текстура заклинания "'..spell..'" не найдена') end
        return false
    end

    -- Если баф уже есть на игроке, не кастуем
    if (RT_BuffHelper.Check(unit, texture)) then
        if RT.Debug then RT.Log('Эффект "'..spell..'" найден') end
        return false
    end

    return RT.CastSpell(spell, unit)
end

-- Кастует заклинание из очереди, если соответствующего эффекта еще нет на цели
-- Пример использования: /run RT.CastDebuffQueue("Corruption","Immolate","Curse of Weakness","Shadow Bolt")
RT.CastDebuffQueue = function(...)

    -- идем по списку заклинаний из переменных функции
    for i=1,arg.n do
        -- Кастуем, если дебафа нет на цели
        if RT.CastDebuff(arg[i]) then
            return 1
        end
    end

    return false
end

-- Кастует заклинание в зависимости от наличия бафа на игроке
-- Пример использования: /run RT.CastIfBuff("Frost Armor", "Fireball", "Cannibalize")
RT.CastIfBuff = function( buff, spell_is_buff, spell_no_buff, unit)

    if not unit then unit="player" end

    if not (buff and spell_is_buff) then
        if RT.Debug then RT.Log('Формат использования: /run RT.CastIfBuff("buff","spell_is_buff","spell_no_buff")') end
        return false
    end

    local texture = RT_SpellHelper.GetTexture(buff)

    if RT.Debug then RT.Log('Текстура бафа: '..texture) end
    if RT_BuffHelper.Check(unit, texture) then
        if RT.Debug then RT.Log('Баф найден, кастуем "'..spell_is_buff..'"') end
        CastSpellByName(spell_is_buff)
    else
        if RT.Debug then RT.Log('Баф не найден, кастуем "'..spell_no_buff..'"') end
        CastSpellByName(spell_no_buff)
    end

    return 1

end

-- Кастует заклинание в зависимости от наличия активного таланта на игроке
-- Пример использования: /run RT.CastIfBuff("Frost Armor", "Fireball", "Cannibalize")
RT.CastIfTalent = function( talent, spell_is_talent, spell_no_talent, unit)

    if not unit then unit="player" end

    if not (talent and spell_is_talent) then
        if RT.Debug then RT.Log('Формат использования: /run RT.CastIfTalent("talent","spell_is_talent","spell_no_talent")') end
        return false
    end

    local texture = RT_TalentHelper.GetTexture(talent)

    if RT.Debug then RT.Log('Текстура таланта: '..texture) end
    if RT_BuffHelper.Check(unit, texture) then
        if RT.Debug then RT.Log('Талант активен, кастуем "'..spell_is_talent..'"') end
        CastSpellByName(spell_is_talent)
    else
        if RT.Debug then RT.Log('Талант не активен, кастуем "'..spell_no_talent..'"') end
        CastSpellByName(spell_no_talent)
    end

    return 1

end

-- Кастует заклинание из очереди, если соответствующего эффекта еще нет на игроке
-- Пример использования: /run RT.CastBuffQueue("Frost Armor","Fireball")
RT.CastBuffQueue = function(...)

    -- идем по списку заклинаний из переменных функции
    for i=1,arg.n do
        -- Кастуем, если бафа нет на игроке
        if RT.CastBuff(arg[i]) then
            return 1
        end
    end

    return false
end

-- Кастует заклинание из очереди, проверяя дистанцию и кулдаун при наличии текстуры в ActionBar
-- Пример использования: /run RT.CastSpellQueue("Shadow Bolt","Fireball")
RT.CastSpellQueue = function(...)

    -- Список действий
    local action_list = RT_ActionHelper.GetList();

    -- идем по списку заклинаний из переменных функции
    for i=1,arg.n do
        -- Кастуем, если получится
        if RT.CastSpell(arg[i]) then
            return 1
        end
    end

    return false
end