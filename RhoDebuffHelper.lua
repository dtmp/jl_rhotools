RT_DebuffHelper = {}

-- Возвращает массив дебафов на цели {текстура,...}
RT_DebuffHelper.GetList = function ( unit )
    local t={}
    local i=1
    while UnitDebuff(unit, i) do
        t[i] = UnitDebuff(unit, i)
        i = i + 1
    end

    return t
end

-- Выводит информацию о дебафах на цели
RT_DebuffHelper.ShowInfo = function ( unit )
    if not unit then unit="target" end
    local t = RT_DebuffHelper.GetList(unit)
    for i,s in pairs(t) do
        RT.Log(s)
    end
end

-- Проверяет есть ли дебаф на цели
RT_DebuffHelper.Check = function ( unit, texture )
    local t = RT_DebuffHelper.GetList(unit)
    for i,s in pairs(t) do
        if s==texture then return 1
        end
    end

    return false
end

