RT_ActionHelper = {}

-- Возвращает массив действий {{slot=>№ слота,texture=>текстура},...}
RT_ActionHelper.GetList = function( index )

    if (not index) then index='texture' end

    local t={}

    local lActionSlot = 0;
    for lActionSlot = 1, 120 do
        --local lActionText = GetActionText(lActionSlot);
        local lActionTexture = GetActionTexture(lActionSlot);
        if lActionTexture then
            local a = {};
            a.slot = lActionSlot;
            a.texture = lActionTexture;

            t[a[index]] = a;
        end
    end

    return t
end

-- Выводит информацию о действиях
RT_ActionHelper.ShowInfo = function ( slot )
    local t = RT_ActionHelper.GetList('slot');
    for i,a in pairs(t) do
        if (not slot or a.slot==slot) then
            RT.Log('Слот '..a.slot..' - '..a.texture)
        end
    end
end

RT_ActionHelper.GetSlotByTexture = function( texture )
    local t = RT_ActionHelper.GetList();
    if (t[texture]) then
        return t[texture].slot;
    else
        if RT.Debug then RT.Log('Не найден слот по текстуре "'..texture..'"') end
        return false
    end
end

-- Возвращает false, если дистанция не позволяет применить заклинание
RT_ActionHelper.IsRange = function( texture )
    local slot = RT_ActionHelper.GetSlotByTexture ( texture )

    if (not ActionHasRange(slot)) then
        if RT.Debug then RT.Log('К действию '..slot..' ("'..texture..'") дистанция не применяется') end
        return 1
    end

    if (IsActionInRange(slot)~=1) then
        if RT.Debug then RT.Log('Слишком далеко для действия '..slot..' ("'..texture..'")') end
        return false
    end

    return 1
end
