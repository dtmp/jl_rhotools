
-- Синонимы функция для обратной совместимости с Rhodzevool
ShowSpellInfo = RT_SpellHelper.ShowInfo
CastDebuff = RT.CastDebuffQueue

-- Синонимы для быстрого доступа
RTSSI = RT_SpellHelper.ShowInfo
RTGC = RT_MsgHelper.GetColorText

-- Синонимы для совместимости с версией <= 0.12
RT.CastSpellDistance = RT.CastSpell
RT.CastSpellDistanceQueue = RT.CastSpellQueue

-- Функции тестирования
RHT = function()
    text = RTGC('Тест', 'Green')..RTGC(' и еще', 'Turquoise')..RTGC('и еще');
    RT.Log(text);
end

dd=RT_MsgHelper.ShowHelp

--fff = function()
--    return 1
--end
--
--ggg = function()
--    f = 1
--    return fff()
--end
--
--ttt = function()
--    if (ggg()) then RT.Log(1) end
--end