
function RhoTools_OnLoad()
    RT.Log("Rhodzevool Tools Loaded (v0.13)")

    -- Register for Loading Variables
    -- self:RegisterEvent( "PLAYER_ENTERING_WORLD" );
    -- self:RegisterEvent( "PLAYER_LEAVING_WORLD" );

end

function RhoTools_OnEvent(event,...)
    RT.Log(event);
end

local Frame = CreateFrame("Frame")
Frame:RegisterEvent("PLAYER_LOGIN")
--Frame:RegisterEvent("SYSMSG")
Frame:RegisterEvent("UI_ERROR_MESSAGE")

Frame:SetScript("OnEvent", function(...)
    if RT.Debug then RT.Log(arg1) end
end)