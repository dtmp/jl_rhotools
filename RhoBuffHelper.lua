RT_BuffHelper = {}

-- Возвращает массив бафов на цели {текстура,...}
RT_BuffHelper.GetList = function ( unit )
    local t={}
    local i=1
    while UnitBuff(unit, i) do
        t[i] = UnitBuff(unit, i)
        i = i + 1
    end

    return t
end

-- Выводит информацию о бафах на цели
RT_BuffHelper.ShowInfo = function ( unit )
    if not unit then unit="player" end
    local t = RT_BuffHelper.GetList(unit)
    for i,s in pairs(t) do
        RT.Log(s)
    end
end

-- Проверяет есть ли баф на цели
RT_BuffHelper.Check = function ( unit, texture )
    local t = RT_BuffHelper.GetList(unit)
    for i,s in pairs(t) do
        if s==texture then return 1
        end
    end

    return false
end
