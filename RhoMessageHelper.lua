
RT_MsgHelper = {}

RT_MsgHelper.GetColorText = function(text, color)

    local color_hex = 'FFFFFF';
    if (RT_ColorHelper[color]) then color_hex=RT_ColorHelper[color] end

    local text = '\124cff'..color_hex..'\124h'..text..'\124h\124r'

    return text
end


-- /run RT_MsgHelper.ShowHelp()
RT_MsgHelper.ShowHelp = function()

    local l = RT.Log;
    local c = RT_MsgHelper.GetColorText;

    local c_spell_name = "Gold";
    local c_spell_param = "LightCoral";
    local c_spell_desc = "PaleGoldenRod";
    local c_spell_example = "SandyBrown";

    local t = ''

    t = t..c("-----------------------------------------------------------------------------", "Yellow").."\n"
    t = t..c("Описание доступных команд Rhodzevool Tools v0.11", "Yellow").."\n"
    t = t..c("-----------------------------------------------------------------------------", "Yellow").."\n"

    t = t..c("\n")
    t = t..c(" | Названия функций ",c_spell_name)..c(" (параметры функций)\n",c_spell_param);
    t = t..c(" | Описание функций\n",c_spell_desc);
    t = t..c(" | Примеры вызова\n",c_spell_example);

    t = t..c("\n")

    t = t..c("---------------------------------", "Khaki").."\n"
    t = t..c("1. Базовые функции", "Khaki").."\n"
    t = t..c("---------------------------------", "Khaki").."\n\n"

    t = t..c("1.1. ", "Khaki")..c("RT.CastSpellDistance ",c_spell_name)..c("(")..c("spell, unit",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание с учетом дистанции и КД.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastSpellDistance(\"Shadow Bolt\")",c_spell_example).."\n\n"

    t = t..c("1.2. ", "Khaki")..c("RT.CastDebuff ",c_spell_name)..c("(")..c("spell, unit",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание, если дебафа еще нет на цели.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastDebuff(\"Corruption\")",c_spell_example).."\n\n"

    t = t..c("1.3. ", "Khaki")..c("RT.CastBuff ",c_spell_name)..c("(")..c("spell, unit",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание, если бафа еще нет на игроке.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastBuff(\"Frost Armor\")",c_spell_example).."\n\n"

    t = t..c("1.4. ", "Khaki")..c("RT.CastIfBuff ",c_spell_name)..c("(")..c("buff, spell_is_buff, spell_no_buff, unit",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание в зависимости от наличия бафа на игроке.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastIfBuff(\"Frost Armor\", \"Fireball\", \"Cannibalize\"))",c_spell_example).."\n\n"

    t = t..c("1.5. ", "Khaki")..c("RT.CastIfTalent ",c_spell_name)..c("(")..c("talent, spell_is_talent, spell_no_talent, unit",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание в зависимости от наличия активного таланта.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastIfBuff(\"Frost Armor\", \"Fireball\", \"Cannibalize\")",c_spell_example).."\n\n"

    t = t..c("---------------------------------", "Khaki").."\n"
    t = t..c("2. Функции очередей", "Khaki").."\n"
    t = t..c("---------------------------------", "Khaki").."\n\n"

    t = t..c("Количество заклинаний в очереди не ограницено. При",c_spell_desc).."\n"
    t = t..c("использовании очереди бафов/дебафов последним заклинанием",c_spell_desc).."\n"
    t = t..c("можно ставить любое атакующее.",c_spell_desc).."\n\n"

    t = t..c("2.1. ", "Khaki")..c("RT.CastSpellDistanceQueue ",c_spell_name)..c("(")..c("...",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание из очереди, проверяя дистанцию и кулдаун. При",c_spell_desc).."\n"
    t = t..c("  испольовании обязательно наличие соответствующего действия в",c_spell_desc).."\n"
    t = t..c("  в Action Bar со стандартной иконкой заклинания.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastSpellDistanceQueue(\"Shadow Bolt\",\"Fireball\")",c_spell_example).."\n\n"

    t = t..c("2.2. ", "Khaki")..c("RT.CastBuffQueue ",c_spell_name)..c("(")..c("...",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание из очереди, если эффекта еще нет на игроке.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastBuffQueue(\"Frost Armor\",\"Fireball\")",c_spell_example).."\n\n"

    t = t..c("2.3. ", "Khaki")..c("RT.CastDebuffQueue ",c_spell_name)..c("(")..c("...",c_spell_param)..c(")").."\n\n"
    t = t..c("  Кастует заклинание из очереди, если эффекта еще нет на цели.",c_spell_desc).."\n"
    t = t..c("  /run RT.CastDebuffQueue(\"Corruption\",\"Immolate\",\"Shadow Bolt\")",c_spell_example).."\n\n"

    t = t..c("---------------------------------", "Khaki").."\n"
    t = t..c("3. Примечания", "Khaki").."\n"
    t = t..c("---------------------------------", "Khaki").."\n\n"

    t = t..c("Любая функция с действием вернет true (1) при успехе",c_spell_desc).."\n"
    t = t..c("и false, если действие не было применено.",c_spell_desc).."\n\n"

    t = t..c("Подробнее познакомиться с аддоном можно, почитав LUA файлы.",c_spell_desc).."\n"

    l ( t )

end

